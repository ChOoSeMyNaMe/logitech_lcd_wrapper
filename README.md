# Logitech_LCD_Wrapper #
A simple C#-Wrapper for the Logitech LCD SDK

## Contents ##
![Alt text](https://bytebucket.org/ChOoSeMyNaMe/logitech_lcd_wrapper/raw/430e20a304e91305812fffb58d05753f646c5abc/classdiagram.png "Class Diagram")

## Usage ##
In order to use this library you have to place the Logitech LCD SDK of the folder of your application.
You can find the SDK in the Programfolder in your Logitech Gaming Software or download it here: 
[Logitech G Developer Lab](http://gaming.logitech.com/en-us/developers)
