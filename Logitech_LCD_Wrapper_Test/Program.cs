﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using Logitech_LCD_Wrapper;

namespace Logitech_LCD_Wrapper_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var applet = new Applet("Test");
            var background = applet.Background;

            var rec = new Rectangle(0, 0, 10, background.LcdSize.Height - 1);

            applet.ButtonPressed += (s, e) =>
            {
                if(e.PressedButton == Buttons.MonoButton0)
                {
                    rec.Width -= 5;
                }
                else if(e.PressedButton == Buttons.MonoButton3)
                {
                    rec.Width += 5;
                }
            };

            for (int i = 0; i < 50; i++)
            {
                using (var g = background.CreateGraphics())
                {
                    g.Clear(Color.Black);
                    g.DrawRectangle(Pens.White, rec);
                }
                applet.SetBackground();
                applet.Update();

                rec = new Rectangle(rec.X + 5, rec.Y, rec.Width, rec.Height);
                if (rec.X >= background.LcdSize.Width)
                    rec.X = 0;
                Thread.Sleep(500);
            }

            applet.SetTitle("Test", false);
            applet.Update();

            Thread.Sleep(5000);

            applet.Dispose();
        }
    }
}
