﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Timers;

namespace Logitech_LCD_Wrapper
{
    /// <summary>
    /// Represents an applet for the Logitech Gaming SDK.
    /// ---Important: The required Logitech Gaming SDK(x64 or x86) should be in the directory of 
    /// your application (Name should be "LogitechLcd.dll": <seealso cref="NativeFunctions.DllPath"/>).
    /// </summary>
    public class Applet : IDisposable
    {
        private LcdType connectedLcdType;
        private Timer timer;

        /// <summary>
        /// Gets the background of the LCD.
        /// Use for drawing!
        /// </summary>
        public LcdBitMap Background { get; private set; }

        /// <summary>
        /// Gets the type of the connected LCD
        /// </summary>
        public LcdType ConnectedLcdType
        {
            get
            {
                if (connectedLcdType == LcdType.None)
                {
                    if (NativeFunctions.LogiLcdIsConnected((int)LcdType.Color))
                        return LcdType.Color;
                    if (NativeFunctions.LogiLcdIsConnected((int)LcdType.Mono))
                        return LcdType.Mono;
                    return LcdType.None;
                }
                return connectedLcdType;
            }
        }

        /// <summary>
        /// Enables or disables the AutoCheck for the LCD-Buttons
        /// </summary>
        public bool AutoCheckButtons
        {
            get => timer.Enabled;
            set => timer.Enabled = value;
        }

        /// <summary>
        /// Gets or sets the AutoCheck-Interval
        /// </summary>
        public int ButtonCheckInterval
        {
            get => (int)timer.Interval;
            set => timer.Interval = value;
        }


        /// <summary>
        /// Raised when a button is pressed
        /// </summary>
        public event EventHandler<ButtonPressedEventArgs> ButtonPressed;


        /// <summary>
        /// Initializes the SDK and creates an new applet.
        /// </summary>
        /// <param name="friendlyName">Name of the Applet</param>
        /// <param name="type">Specify the type of the LCD manually</param>
        /// <param name="autoCheckButtons">Enables or disables the AutoCheck for the LCD-Buttons</param>
        public Applet(string friendlyName, LcdType type = LcdType.Color | LcdType.Mono, bool autoCheckButtons = true)
        {
            connectedLcdType = LcdType.None;
            if(!NativeFunctions.LogiLcdInit(friendlyName, (int)type))
            {
                throw new Exception("Unable to initialize the SDK. Please check your parameters!");
            }
            Background = new LcdBitMap(ConnectedLcdType);

            timer = new Timer(200);
            timer.AutoReset = true;
            timer.Elapsed += OnCheckButtons;
            timer.Enabled = autoCheckButtons;

            AppDomain.CurrentDomain.ProcessExit += (s, e) => Dispose();
        }

        /// <summary>
        /// Checks the state of each button and raises an event if a button is being pressed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnCheckButtons(object sender, ElapsedEventArgs e)
        {
            CheckButtons();
        }

        /// <summary>
        /// Checks the state of each button and raises an event if a button is being pressed.
        /// </summary>
        public virtual void CheckButtons()
        {
            var values = (Buttons[])Enum.GetValues(typeof(Buttons));

            foreach (var button in values)
            {
                if (NativeFunctions.LogiLcdIsButtonPressed((int)button))
                    ButtonPressed?.Invoke(this, new ButtonPressedEventArgs(button));
            }
        }

        /// <summary>
        /// Sends the Background to the LCD
        /// </summary>
        /// <returns>If it succeded</returns>
        public bool SetBackground()
        {
            if(ConnectedLcdType == LcdType.Color)
            {
                return NativeFunctions.LogiLcdColorSetBackground(Background.GetPixels());
            }
            else
            {
                return NativeFunctions.LogiLcdMonoSetBackground(Background.GetPixels());
            }
        }

        /// <summary>
        /// Updates the LCD. Needed after <seealso cref="SetBackground"/>, <seealso cref="SetText"/> or <seealso cref="SetTitle"/>.
        /// </summary>
        public void Update()
        {
            NativeFunctions.LogiLcdUpdate();
        }

        /// <summary>
        /// Shows the text in the specified line in <seealso cref="Color.Black"/>
        /// </summary>
        /// <param name="line">Linenumber</param>
        /// <param name="text">Text that should be displayed</param>
        /// <returns>If it succeded</returns>
        public bool SetText(int line, string text)
        {
            return SetText(line, text, Color.Black);
        }

        /// <summary>
        /// Shows the text in the specified line with the given Color
        /// </summary>
        /// <param name="line">Linenumber</param>
        /// <param name="text">Text that should be displayed</param>
        /// <param name="foreColor">Color of the text</param>
        /// <returns>If it succeded</returns>
        public bool SetText(int line, string text, Color foreColor)
        {
            if (ConnectedLcdType == LcdType.Color)
            {
                return NativeFunctions.LogiLcdColorSetText(line, text, foreColor.R, foreColor.G, foreColor.B);
            }
            else
            {
                return NativeFunctions.LogiLcdMonoSetText(line, text);
            }
        }

        /// <summary>
        /// Sets the title of the LCD in <seealso cref="Color.Black"/>
        /// </summary>
        /// <param name="title">The title which should be displayed</param>
        /// <param name="ignoreIfMono">Don't set a title if <seealso cref="ConnectedLcdType"/> = <seealso cref="LcdType.Mono"/>,
        /// else write the title in the first line of the Monochrome-LCD</param>
        /// <returns>If it succeded</returns>
        public bool SetTitle(string title, bool ignoreIfMono = true)
        {
             return SetTitle(title, Color.Black, ignoreIfMono);
        }

        /// <summary>
        /// Sets the title of the LCD in <seealso cref="Color.Black"/>
        /// </summary>
        /// <param name="title">The title which should be displayed</param>
        /// <param name="foreColor">Color of the title</param>
        /// <param name="ignoreIfMono">Don't set a title if <seealso cref="ConnectedLcdType"/> = <seealso cref="LcdType.Mono"/>,
        /// else write the title in the first line of the Monochrome-LCD</param>
        /// <returns>If it succeded</returns>
        public bool SetTitle(string title, Color foreColor, bool ignoreIfMono = true)
        {
            if(ConnectedLcdType == LcdType.Color)
            {
                return NativeFunctions.LogiLcdColorSetTitle(title, foreColor.R, foreColor.G, foreColor.B);
            }
            else if(!ignoreIfMono)
            {
                return SetText(0, title);
            }
            return false;
        }

        /// <summary>
        /// Frees all memory used by the SDK and the Applet
        /// </summary>
        public void Dispose()
        {
            timer?.Dispose();
            Background?.Dispose();
            NativeFunctions.LogiLcdShutdown();
        }
    }
}
