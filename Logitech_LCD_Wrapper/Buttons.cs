﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logitech_LCD_Wrapper
{
    [Flags]
    public enum Buttons
    {
        ColorLeft = 0x00000100,
        ColorRight = 0x00000200,
        ColorOk = 0x00000400,
        ColorCancel = 0x00000800,
        ColorUp = 0x00001000,
        ColorDown = 0x00002000,
        ColorMenu = 0x00004000,


        MonoButton0 = 0x00000001,
        MonoButton1 = 0x00000002,
        MonoButton2 = 0x00000004,
        MonoButton3 = 0x00000008
    }
}
