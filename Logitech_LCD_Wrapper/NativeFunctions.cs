﻿using System;
using System.Runtime.InteropServices;

namespace Logitech_LCD_Wrapper
{
    public class NativeFunctions
    {
        public const int Lcd_Color_Width = 320;
        public const int Lcd_Color_Height = 240;
        public const int Lcd_Color_BytesPerPixel = 4;

        public const int Lcd_Mono_Width = 160;
        public const int Lcd_Mono_Height = 43;
        public const int Lcd_Mono_BytesPerPixel = 1;

        public const string DllPath = @"LogitechLcd.dll";

        /// <summary>
        /// Makes necessary initializations. Has to be called first in order to use any other function.
        /// </summary>
        /// <param name="friendlyName">The name of your applet</param>
        /// <param name="lcdType">The type of your target lcd device 
        /// (<seealso cref="LcdType.Mono"/>, <seealso cref="LcdType.Color"/> or <seealso cref="LcdType.Mono"/> | <seealso cref="LcdType.Color"/>)</param>
        /// <returns></returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdInit(string friendlyName, int lcdType);

        /// <summary>
        /// Checks if a device of the specified type if connected
        /// </summary>
        /// <param name="lcdType"><seealso cref="LcdType.Mono"/>, <seealso cref="LcdType.Color"/> or <seealso cref="LcdType.Mono"/> | <seealso cref="LcdType.Color"/></param>
        /// <returns>True if a device is found, False if not found or <seealso cref="LogiLcdInit(string, int)"/> has not been called before.</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdIsConnected(int lcdType);

        /// <summary>
        /// Kills the applet and frees the memory used by the SDK
        /// </summary>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void LogiLcdShutdown();


        /// <summary>
        /// Checks if the specified key is being pressed
        /// </summary>
        /// <param name="button">The button which should be checked</param>
        /// <returns>If it is being pressed</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdIsButtonPressed(int button);

        /// <summary>
        /// Updates the LCD
        /// </summary>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern void LogiLcdUpdate();


        /// <summary>
        /// Sets the specified image as background
        /// Image-Dimensions: 160x43 (<seealso cref="Lcd_Mono_Width"/> x <seealso cref="Lcd_Mono_Height"/>)
        /// </summary>
        /// <param name="bitmap">1-Byte per Pixel (<seealso cref="Lcd_Mono_BytesPerPixel"/>)</param>
        /// <returns>If it succeds</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdMonoSetBackground(byte[] bitmap);

        /// <summary>
        /// Sets the text in the specified line on the monochrome LCD
        /// </summary>
        /// <param name="line">The line where the text should appear</param>
        /// <param name="text">The text that will be displayed</param>
        /// <returns>If it succeds</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdMonoSetText(int line, string text);


        /// <summary>
        /// Sets the specified image as background
        /// Image-Dimensions: 320x240 (<seealso cref="Lcd_Color_Width"/> x <seealso cref="Lcd_Color_Height"/>)
        /// </summary>
        /// <param name="bitmap">4-Bytes per Pixel (<seealso cref="Lcd_Color_BytesPerPixel"/>) in the following order: Blue, Green, Red, Alpha</param>
        /// <returns>If it succeds</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdColorSetBackground(byte[] bitmap);

        /// <summary>
        /// Sets the first line on the colored LCD
        /// </summary>
        /// <param name="title">The text that will be displayed</param>
        /// <param name="red">Red-Gamma</param>
        /// <param name="green">Green-Gamma</param>
        /// <param name="blue">Blue-Gamma</param>
        /// <returns>If it succeds</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdColorSetTitle(string title, int red, int green, int blue);

        /// <summary>
        /// Sets the text in the specified line on the colored LCD
        /// </summary>
        /// <param name="line">The line where the text should appear</param>
        /// <param name="text">The text that will be displayed</param>
        /// <param name="red">Red-Gamma</param>
        /// <param name="green">Green-Gamma</param>
        /// <param name="blue">Blue-Gamma</param>
        /// <returns>If it succeds</returns>
        [DllImport(DllPath, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        public static extern bool LogiLcdColorSetText(int line, string text, int red, int green, int blue);
    }
}
