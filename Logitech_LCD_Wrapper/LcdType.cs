﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logitech_LCD_Wrapper
{
    [Flags]
    public enum LcdType
    {
        Mono = 0x00000001,
        Color = 0x00000002,
        None = -1
    }
}
