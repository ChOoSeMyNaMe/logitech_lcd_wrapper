﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logitech_LCD_Wrapper
{
    public class ButtonPressedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the pressed Button
        /// </summary>
        public Buttons PressedButton { get; private set; }

        public ButtonPressedEventArgs(Buttons pressedButton)
        {
            PressedButton = pressedButton;
        }
    }
}
