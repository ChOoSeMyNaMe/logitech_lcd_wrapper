﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

namespace Logitech_LCD_Wrapper
{
    /// <summary>
    /// Used for the background of the LCD
    /// </summary>
    public class LcdBitMap : IDisposable
    {
        /// <summary>
        /// Gets the Screen-Size of the connected LCD
        /// </summary>
        public Size LcdSize { get; private set; }

        /// <summary>
        /// Gets the type of the connected LCD
        /// </summary>
        public LcdType LcdType { get; private set; }

        /// <summary>
        /// Gets the Bitmap which is used for drawing
        /// </summary>
        public Bitmap Bitmap { get; private set; }
        

        public LcdBitMap(LcdType type)
        {
            LcdType = type;
            if(type == LcdType.Color)
            {
                LcdSize = new Size(NativeFunctions.Lcd_Color_Width, NativeFunctions.Lcd_Color_Height);
            }
            else
            {
                LcdSize = new Size(NativeFunctions.Lcd_Mono_Width, NativeFunctions.Lcd_Mono_Height);
            }

            Bitmap = new Bitmap(LcdSize.Width, LcdSize.Height, PixelFormat.Format32bppArgb);
        }

        /// <summary>
        /// Creates an Graphics-Object for drawing
        /// </summary>
        /// <returns></returns>
        public Graphics CreateGraphics()
        {
            return Graphics.FromImage(Bitmap);
        }

        /// <summary>
        /// Converts an array of colored pixels(BGRA-Format) into an array of monochrome pixels
        /// Override function for customized handling
        /// </summary>
        /// <param name="pixels">Colored Pixel-Array</param>
        /// <returns>Monochrome Pixel-Array</returns>
        protected virtual byte[] ConvertToMonochrome(byte[] pixels)
        {
            byte[] result = new byte[pixels.Length / 4];
            for(int i = 0; i < result.Length; i++)
            {
                result[i] = pixels[i * 4];
            }
            return result;
        }

        /// <summary>
        /// Gets an array of pixels from the Bitmap in the required format
        /// </summary>
        /// <returns>Array of pixel (Monochrome or Colored)</returns>
        public byte[] GetPixels()
        {
            byte[] pixels = new byte[LcdSize.Width * LcdSize.Height * 4];

            var rec = new Rectangle(0, 0, LcdSize.Width, LcdSize.Height);
            BitmapData bmpData = Bitmap.LockBits(rec, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            IntPtr ptr = bmpData.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(ptr, pixels, 0, pixels.Length);
            Bitmap.UnlockBits(bmpData);

            if(LcdType == LcdType.Mono)
            {
                pixels = ConvertToMonochrome(pixels);
            }
            return pixels;
        }

        /// <summary>
        /// Frees used memory
        /// </summary>
        public void Dispose()
        {
            Bitmap?.Dispose();
        }
    }
}
